# sid (Qubole on Docker)

sid (Spark In Docker) is simple docker based tool to setup developer environment for building and running entire Spark services on developer box. The services includes zeppelin/spark/yarn/hdfs/mysql(hive store).


### Prerequisite

### Docker

1. **Install**: For mac install docker hub from "https://docs.docker.com/docker-for-mac/install/". 
2. **Setup**:        
        1. `docker swarm init`  (initialize docker swarm)  
        2. `docker run -d -p 5000:5000 --restart=always --name registry registry:2` (Initialize local docker registry to store docker images)

### jq

Install jq for config parsing. For mac install from  using `brew install jq`

### python

Install python for config setter function.

## ENV

All the scripts run in bash so install bash and add following lines in your ~/.bashrc script.

1. `UTILS=<This git repo location>`
2. `PATH=${PATH}:${UTILS}/bin`
3. `source ~/.bashrc`


## CONFIG

Create config file `config.json` which specs out the container and runtime configuration.

```
{
	"rm": {
		"config" : {
			"mapred.job.hustler.enabled":false
		}
	},
	"nm": {
		"count":2,
		"config" : {
			"mapred.job.hustler.enabled":false
		}
	}
	"dn": {
		"count":1,
	}
}
```


## Workflow

This is typical workflow to setup and start working with sid environment.

**When setting up first time create docker image**

```bash
sid <config_file> image create
```

**Set up ENV. Based on spec in config.json i.e env name / platform / type (cloning, linking, copying source code) setup source code**

```bash
sid <config_file> source setup`
```

**Setup container environment for building source code and running services.**

```bash
sid <config_file> container start
```

**Inspect and see entire setup**
```bash
sid <config_file> container inspect
```

**Build the code. You can optionally build hadoop/hive/spark/zeppelin as well.**

```bash
sid <config_file> source build all
```

**Start all services. You can optionally start rm,nm,nn,dn,zeppelin as well.**

```bash
sid <config_file> service start all
```

**Run spark against this setup**

```bash
sid <config_file> spark
```

_Run time configuration for running service needs to passed in config.json file_

**Container Layout**

Once the containers are started the layout is as follows

```bash
               Master                                     Collection of Yarn Node Managers
 +--------------------------------------+           +------------+  +------------+  +------------+
 |                                      |           |            |  |            |  |            |
 |                                      |           |            |  |            |  |            |
 |                                      |           |            |  |            |  |            |
 |      1. Yarn Resource Manager        |           |            |  |            |  |            |     - - -
 |      2. HDFS Name Node               |           +------------+  +------------+  +------------+
 |      3. Hive Metastore MySQL         |
 |      4. Zeppelin Server              |                 Collection of HDFS Data Nodes
 |                                      |           +------------+  +------------+  +------------+
 |      * User Spark Driver *           |           |            |  |            |  |            |
 |      1. Sparkshell                   |           |            |  |            |  |            |
 |      2. Notebook                     |           |            |  |            |  |            |     - - -
 |                                      |           |            |  |            |  |            |
 |                                      |           +------------+  +------------+  +------------+
 |                                      |
 |                                      |                                Dev
 |                                      |           +-------------------------------------------+
 |                                      |           |          1.Box to build source            |
 |                                      |           |                                           |
 |                                      |           |                                           |
 |                                      |           |                                           |
 +--------------------------------------+           +-------------------------------------------+


                                         INTERFACE
-----------------------------------------------------------------------------------------------------------
                        1. All sources/.m2 are mounted on docker containers
						2. Docker container expose SSH/UI Endpoints/Debug endpoint
-----------------------------------------------------------------------------------------------------------

  									     USER LAPTOP
					 +---------------------------------------------------------+
					 |     1.  All the source code                             |
					 |     2.  .m2 with all the dependency JARS                |
					 |     3.  Docker server and registry                      |
					 |                                                         |
					 +---------------------------------------------------------+
```

**Use `sid help` to see all the commands

