
# Presumed standard export for this build env.
export HADOOP_PREFIX=/usr/lib/hadoop2
export HADOOP_COMMON_HOME=/usr/lib/hadoop2
export HADOOP_HDFS_HOME=/usr/lib/hadoop2
export HADOOP_MAPRED_HOME=/usr/lib/hadoop2
export HADOOP_YARN_HOME=/usr/lib/hadoop2
export YARN_USER_CLASPATH=/usr/lib/hadoop2/share/hadoop/qubole/*:/usr/lib/hadoop2/share/hadoop/qubole/lib/*

# Docker container specific predefined exports

# 1. Conf directory per container
#    all *site.xml / spark-default.sh / spark-env.sh etc come here.
#    Any future config or non-sharable file should be copid in here.
export HADOOP_CONF_DIR=/conf
export YARN_CONF_DIR=/conf

# 2. Predefined mount points and required path for the build script
#    to perform proper build.
declare -x HADOOP_SRC="/codeline/hadoop2"
declare -x SPARK_SRC="/codeline/spark"
declare -x HUSTLER_SRC="/codeline/hustler"
declare -x HIVE_SRC="/codeline/hive"
declare -x ZEPPELIN_SRC="/codeline/zeppelin"
declare -x UTILS="/build-utils"

# 3. Predefind location where maven is intalled when creating docker
#    container.
export M2_HOME=/opt/maven

export PATH=$HADOOP_PREFIX/bin:$HADOOP_PREFIX/sbin:${UTILS}/bin:${M2_HOME}/bin:${PATH}

# Hack for docker somehow $USER is not set
export USER=`whoami`

# Visual Beauty
declare -x CLI_COLOR="1"
declare -x COLORTERM="truecolor"
declare -x LSCOLORS=ExGxBxDxCxEgEdxbxgxcxd
declare -x TERM="xterm-256color"

# source build utils utility functions.
. ${UTILS}/bin/utils
