#!/usr/local/bin/python
import sys
import xml.dom.minidom
def fetch(f, propertyName):
    with open(f, "r") as raw_xml:
        parsed_xml = xml.dom.minidom.parse(raw_xml)
        config = parsed_xml.documentElement
        for property in parsed_xml.getElementsByTagName("property"):
            if propertyName == property.getElementsByTagName("name")[0].firstChild.nodeValue:
                return property.getElementsByTagName("value")[0].firstChild.nodeValue

print fetch("core-site.xml", "master.hostname")
