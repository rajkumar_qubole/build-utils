#!/bin/bash

. docker_local
################################################################################
##############          Utility Functions         ##############################
################################################################################
isbuilt() {
	SRC=${1}
	case ${SRC} in
		(hadoop)
			HADOOP_JAR=$(basename -- `ls ${HADOOP_SRC}/hadoop-dist/target/hadoop-dist*.jar 2>/dev/null` 2>/dev/null)
			if [ $? -ne 0 ]
			then
				return 1
			fi
			return 0
			;;
		(hive)
			ls ${HIVE_SRC}/packaging/target/apache-hive-*bin/apache-hive-*bin &>/dev/null
			if [ $? -ne 0 ]
			then
				return 1
			fi
			return 0
			;;
	esac
	return 0
}

hversion() {
	### Verify Build is successful
	HADOOP_JAR=$(basename -- `ls ${HADOOP_SRC}/hadoop-dist/target/hadoop-dist*.jar 2>/dev/null` 2>/dev/null)
	if [ $? -ne 0 ]
	then
		echo "HADOOP not built. No Jar found at ${HADOOP_SRC}"
		return
	fi

	HADOOP2_VERSION=`echo ${HADOOP_JAR%.jar} | cut -d'-' -f3-`
	return
}

hsnapshot() {
	HADOOP_SNAPSHOT=`ls ${HADOOP_SRC}/hadoop-dist/target/hadoop* | grep SNAPSHOT: | cut -d':' -f1`
}

hivesnapshot() {
	loc=`ls ${HIVE_SRC}/packaging/target/apache-hive* |grep bin |grep -v ':'`
	HIVE_SNAPSHOT=${HIVE_SRC}/packaging/target/${loc}/${loc}/
}

container_started() {
	return $(docker_local_is_setup $@)
}

container_dev_started() {
	return $(docker_local_dev_is_setup)
}

container_master_setup() {
	return $(docker_local_master_is_setup)
}

container_nm_setup() {
	return $(docker_local_nm_is_setup $@)
}

container_dn_setup() {
	return $(docker_local_dn_is_setup $@)
}

setupQENVbase() {

	if [[ ! -d $QENV_MOUNT_BASE ]]
	then
		echo "Creating base mount directory ${QENV_MOUNT_BASE}"
		mkdir $QENV_MOUNT_BASE
	fi

	# Utils should always be set.
	if [ -z "${UTILS}" ]
	then
		echo "UTILS not set. Please set it to point to util"
		return
	fi

	cp ${UTILS}/settings/* ~/.m2/
}


echoe () {
	OIFS=${IFS}
	IFS='%'
	echo -e $@
	IFS=${OIFS}
}

eg='\033[0;32m'
er='\033[0;31m'
eb='\033[0;34m'
enc='\033[0m'
prn_row()  {
	printf '%-24s | %-10s | %-16s | %-128s\n' "${1}" "${2}" "${3}" "${4}"
}

prn() {
	echoe "${eg} >> ${1}${enc}"
}

bprn() {
	echoe "${eb} >> ${1}${enc}"
}

gprn() {
	echoe "${eg} >> ${1}${enc}"
}

rprn() {
	echoe "${er} >> ${1}${enc}"
}

prn_header() {
	echoe "${eg} > ${1}${enc}"
}

prn_hr() {
	echo
	echo
	printf "%0.s=" {1..150}
	echo
}

prn_ls() {
	printf "%0.s-" {1..150}
	echo
}

helps() {
	OIFS=${IFS}
	IFS='%'
	echo ${1} | sed -e 's/\\n/\\n\\t/g'
	IFS=${OIFS}
}

# Get config from passed in config.json
config_file_is_valid() {
	CONF_FILE=${1}
	VAL=`cat ${CONF_FILE} 2>/dev/null | jq ".name" 2>/dev/null`
	if [[ $? -ne 0 || ${VAL} == "null" ]]
	then
		echoe "Invalid config file ${CONF_FILE}"
		exit
	fi
}



# Get config from passed in config.json
config_get() {
	CONFIG=${1}
	FIELD=${2}
	DEFAULT=${3}
	VAL=$(echo ${CONFIG} | jq ${FIELD})
	if [[ ${VAL} == "null" ]]
	then
		VAL=${DEFAULT}
	fi

	if [[ -z ${VAL} ]]
	then
		echoe "Config ${FIELD} not found in file ${CONFIG_FILE}"
		exit
	fi
	echo ${VAL} | sed -e 's/^"//' -e 's/"$//'
}

# Set config in *site.xml file from passed in config file
config_set() {
	CONFIG=${1}
	FIELD="${2}"
	NAME=${3}
	keys=`jq -r "${FIELD}|keys[]" ${CONF_FILE}`
	for key in ${keys}
	do
		KEY=${key}
		VALUE=$(jq "${FIELD}[\"$key\"]" ${CONF_FILE})
		docker_local_exec ${NAME} "python ${QENV_HOST_SPARK}/qubole_assembly/hustler/populate_xml_props.py ${KEY}=${VALUE} ${QENV_HOST_CONF}/ false 1>/dev/null"
	done
}
